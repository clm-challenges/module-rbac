variable "namespace" {
  type        = string
  description = "Name of the namespace, must be unique."
}
