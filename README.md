# Description

Manages kubernetes\_role and kubernetes\_role\_binding.

### Usage

```hcl
data "google_client_config" "provider" {}

provider "google" {
  project = var.project
  region  = var.region
}

module "rbac" {
  source    = "git::https://gitlab.com/clm-challenges/module-rbac.git"
  namespace = var.namespace
}
```

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [kubernetes_role.podwatch](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role) | resource |
| [kubernetes_role_binding.watchpods](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role_binding) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Name of the namespace, must be unique. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_namespace"></a> [namespace](#output\_namespace) | Name of the namespace, must be unique. |
