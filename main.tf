/**
* # Description
* 
* Manages kubernetes_role and kubernetes_role_binding.
* 
* ### Usage
* 
* ```hcl
* data "google_client_config" "provider" {}
*
* provider "google" {
*   project = var.project
*   region  = var.region
* }
*
* module "rbac" {
*   source    = "git::https://gitlab.com/clm-challenges/module-rbac.git"
*   namespace = var.namespace
* }
* ```
*/
resource "kubernetes_role" "podwatch" {
  metadata {
    name      = "role-pod-watch"
    namespace = var.namespace
    labels = {
      test = "pod-watch"
    }
  }

  rule {
    api_groups = ["", "extensions", "apps"]
    resources  = ["pods"]
    verbs      = ["watch"]
  }
}

resource "kubernetes_role_binding" "watchpods" {
  metadata {
    name      = "role-binding-watch-pods"
    namespace = var.namespace
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = kubernetes_role.podwatch.metadata[0].name
  }
  subject {
    kind      = "User"
    name      = "user-clm"
    api_group = "rbac.authorization.k8s.io"
  }
  #   subject {
  #     kind      = "ServiceAccount"
  #     name      = "default"
  #     namespace = "kube-system"
  #   }
  #   subject {
  #     kind      = "Group"
  #     name      = "system:masters"
  #     api_group = "rbac.authorization.k8s.io"
  #   }
}
