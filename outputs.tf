output "namespace" {
  description = "Name of the namespace, must be unique."
  value       = kubernetes_role_binding.watchpods.metadata[0].namespace
}
